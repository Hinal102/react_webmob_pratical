import React , {Component} from 'react';
import movieimg4 from '../asstes/4.jpeg';
import movieimg5 from '../asstes/5.jpeg';
import movieimg6 from '../asstes/6.jpeg';


class Upcoming extends React.Component {
   movie4() {
      alert(  ' Bhool bhulaiya 2 (2020) \n\n\n' 
      + '07/19/2019 (IN) Adventure, Family, Music 1h 58m \n\n' 
       
      + 'Overview\n\n' 
      
      + 'Simba idolizes his father, King Mufasa, and takes to heart his own royal destiny. But not everyone in the kingdom celebrates the new cubs arrival. Scar, Mufasas brother—and former heir to the throne—has plans of his own. The battle for Pride Rock is ravaged with betrayal, tragedy and drama, ultimately resulting in Simbas exile. With help from a curious pair of newfound friends, Simba will have to figure out how to grow up and take back what is rightfully his.\n\n\n' 
      + "Jon Favre\n''Directo"
      );
    }
    movie5() {
      alert(  'The Kings Man(2020) \n\n\n' 
      + '07/19/2019 (IN) Adventure, Family, Music 1h 58m \n\n' 
       
      + 'Overview\n\n' 
      
      + 'Simba idolizes his father, King Mufasa, and takes to heart his own royal destiny. But not everyone in the kingdom celebrates the new cubs arrival. Scar, Mufasas brother—and former heir to the throne—has plans of his own. The battle for Pride Rock is ravaged with betrayal, tragedy and drama, ultimately resulting in Simbas exile. With help from a curious pair of newfound friends, Simba will have to figure out how to grow up and take back what is rightfully his.\n\n\n' 
      + "Jon Favre\nDirector"
      );
    }
    movie6() {
      alert(  ' Radhe (2020) \n\n\n' 
      + '07/19/2019 (IN) Adventure, Family, Music 1h 58m \n\n' 
       
      + 'Overview\n\n' 
      
      + 'Simba idolizes his father, King Mufasa, and takes to heart his own royal destiny. But not everyone in the kingdom celebrates the new cubs arrival. Scar, Mufasas brother—and former heir to the throne—has plans of his own. The battle for Pride Rock is ravaged with betrayal, tragedy and drama, ultimately resulting in Simbas exile. With help from a curious pair of newfound friends, Simba will have to figure out how to grow up and take back what is rightfully his.\n\n\n' 
      + "Jon Favre\nDirector"
      );
    }
   render() {
      return (
        

<div className="container">
   <h2 className="latest-moive">Upcoming Movie</h2>
   <div className="row">
      <div className="col">
         <img src={movieimg4} className="img-class" onClick={this.movie4} /><br/>
         <p><b>Bhool bhulaiya 2</b></p>
         <span>Upcoming..</span>
      </div>
      <div className="col">
         <img src={movieimg5} className="img-class" onClick={this.movie5} /><br/>
         <p><b>The King's Man </b></p>
         <span>Upcoming..</span>
      </div>
      <div className="col">
         <img src={movieimg6} className="img-class" onClick={this.movie6} /><br/>
         <p><b>Radhe </b></p>
         <span>Upcoming..</span>
      </div>
   </div>
</div>


      )
   }
}
export default Upcoming;