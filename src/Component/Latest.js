import React , {Component} from 'react';
import movieimg1 from '../asstes/1.jpeg';
import movieimg2 from '../asstes/2.jpeg';
import movieimg3 from '../asstes/3.jpeg';


class Latest extends React.Component {
   movie1() {
      alert(  ' Chhichhore (2019) \n\n\n' 
      + '07/19/2019 (IN) Adventure, Family, Music 1h 58m \n\n' 
       
      + 'Overview\n\n' 
      
      + 'Simba idolizes his father, King Mufasa, and takes to heart his own royal destiny. But not everyone in the kingdom celebrates the new cubs arrival. Scar, Mufasas brother—and former heir to the throne—has plans of his own. The battle for Pride Rock is ravaged with betrayal, tragedy and drama, ultimately resulting in Simbas exile. With help from a curious pair of newfound friends, Simba will have to figure out how to grow up and take back what is rightfully his.\n\n\n' 
      + "Jon Favre\n''Directo"
      );
    }
    movie2() {
      alert(  'Aquaman(2018) \n\n\n' 
      + '07/19/2019 (IN) Adventure, Family, Music 1h 58m \n\n' 
       
      + 'Overview\n\n' 
      
      + 'Simba idolizes his father, King Mufasa, and takes to heart his own royal destiny. But not everyone in the kingdom celebrates the new cubs arrival. Scar, Mufasas brother—and former heir to the throne—has plans of his own. The battle for Pride Rock is ravaged with betrayal, tragedy and drama, ultimately resulting in Simbas exile. With help from a curious pair of newfound friends, Simba will have to figure out how to grow up and take back what is rightfully his.\n\n\n' 
      + "Jon Favre\nDirector"
      );
    }
    movie3() {
      alert(  ' Tanhaji (2020) \n\n\n' 
      + '07/19/2019 (IN) Adventure, Family, Music 1h 58m \n\n' 
       
      + 'Overview\n\n' 
      
      + 'Simba idolizes his father, King Mufasa, and takes to heart his own royal destiny. But not everyone in the kingdom celebrates the new cubs arrival. Scar, Mufasas brother—and former heir to the throne—has plans of his own. The battle for Pride Rock is ravaged with betrayal, tragedy and drama, ultimately resulting in Simbas exile. With help from a curious pair of newfound friends, Simba will have to figure out how to grow up and take back what is rightfully his.\n\n\n' 
      + "Jon Favre\nDirector"
      );
    }
   render() {
      return (
        

<div className="container">
   <h2 className="latest-moive">Latest Movie</h2>
   <div className="row">
      <div className="col">
         <img src={movieimg1} className="img-class" onClick={this.movie1} /><br/>
         <p><b>Chhichhore </b></p>
         <span>13 June 2019</span>
      </div>
      <div className="col">
         <img src={movieimg2} className="img-class" onClick={this.movie2} /><br/>
         <p><b>Aquaman </b></p>
         <span>13 July 2018</span>
      </div>
      <div className="col">
         <img src={movieimg3} className="img-class" onClick={this.movie3} /><br/>
         <p><b>Tanhaji </b></p>
         <span>16 January 2019</span>
      </div>
   </div>
</div>


      )
   }
}
export default Latest;
