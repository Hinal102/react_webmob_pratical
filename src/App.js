import React from 'react';
import { BrowserRouter as Router, Route, Link, Switch } from 'react-router-dom'; 
import logo from './logo.svg';
import './App.css';
import Playing from './Component/Playing';
import Latest from './Component/Latest';
import Upcoming from './Component/Upcoming';

function App() {
  return (
    <Router>
    <div className="App">
      <nav className="navbar navbar-expand-sm  nav-color">

  <a class="navbar-brand" href="#">Logo</a>
  
 
  <ul class="navbar-nav">
    <li class="nav-item">
      <a class="nav-link" href="/latest" to="/latest">Latest </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="/playing"  to="/playing">Now Playing</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="/upcoming" to="/upcoming">Upcoming</a>
    </li>
  </ul>
</nav>
<Switch> 
              <Route exact path='/playing' component={Playing}></Route> 
              <Route exact path='/latest' component={Latest}></Route> 
              <Route exact path='/upcoming' component={Upcoming}></Route> 
            </Switch> 

    </div>
    </Router>
  );
}

export default App;
